
var drawing = false,
    filling = false;
var c = document.getElementById("myCanvas");
var ctx = c.getContext('2d');
var color_picker = document.getElementById("colorpicker"),
    brush_size = document.getElementById("brushsize"),
    font_selecter = document.getElementById("fontselecter");
var color = "#000000",
    size = "3",
    txt_font = "serif",
    txt_content ="";
var posX, posY;
var ImageArray = new Array();
ImageArray.push(ctx.getImageData(0, 0, c.width, c.height));
var step = 0;
var mode = "pen";
var img_before_txt;



c.addEventListener("mousedown", ()=>{
    posX = event.offsetX;
    posY = event.offsetY;
    ctx.moveTo(posX, posY);
    drawing = true;
    ctx.strokeStyle = color;
    ctx.lineCap = 'round'
    if(filling) ctx.fillStyle = color;
    else ctx.lineWidth = size;
    if(mode == "pen"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.beginPath();
        
    }
    else if(mode == "eraser"){
        ctx.globalCompositeOperation = 'destination-out';
        ctx.beginPath();
    }
    else if(mode == "rec") ctx.beginPath();
    else if(mode == "text") ctx.globalCompositeOperation = 'source-over';
});
c.addEventListener("mousemove", ()=>{
    if(mode == "pen"){
        if(drawing){ 
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.stroke();
        }
    }
    else if(mode == "eraser"){
        if(drawing){
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.stroke();
        }
    }
    else if(mode == "rec"){
        if(drawing){
            ctx.globalCompositeOperation = 'source-over';
            ctx.clearRect(0, 0, c.width, c.height);
            load_curImage();
            
            if(filling) ctx.fillRect(posX, posY, event.offsetX-posX, event.offsetY-posY);
            else ctx.strokeRect(posX, posY, event.offsetX-posX, event.offsetY-posY);
            
        }
    }
});
c.addEventListener("mouseup", ()=>{
    var x = event.offsetX;
    var y = event.offsetY;
    if(mode == "circle"){
        if(drawing){
            ctx.globalCompositeOperation = 'source-over';
            ctx.beginPath();
            var radius = Math.sqrt((x-posX)*(x-posX) + (y-posY)*(y-posY))/2;
            ctx.arc((x+posX)/2, (y+posY)/2, radius, 0, 2 * Math.PI, false);
            ctx.closePath();
            if(filling) ctx.fill();
            else ctx.stroke();
        }
    }
    else if(mode == "tri"){
        if(drawing){
            ctx.globalCompositeOperation = 'source-over';
            
            
            ctx.beginPath();
            ctx.moveTo((posX+x)/2, posY);
            ctx.lineTo(posX, y);
            ctx.moveTo((posX+x)/2, posY);
            ctx.lineTo(x, y); 
            ctx.moveTo(posX, y);
            ctx.lineTo(x, y);
            ctx.closePath();
            
            if(filling) ctx.fill();
            else ctx.stroke();
        }
    }
    else ctx.closePath();

    if(mode != "text"){ 
        drawing = false;
        pushImage();
    }
});
c.addEventListener("click", ()=>{
    if(mode == "text"){
        txt_content ="";
        ctx.fillStyle = "#faebd7";
        ctx.fillRect(0, 0, c.width, c.height)
        ctx.fillStyle = color;
        ctx.font = size+"px "+txt_font;
        ctx.textAlign = "left";
        ctx.textBaseline = "middle";
        img_before_txt = ImageArray[step];
        ctx.clearRect(0, 0, c.width, c.height);
        ctx.putImageData(img_before_txt, 0, 0);
    }
});
c.addEventListener("mouseout", ()=>{
    drawing = false;
});
window.addEventListener("keydown", ()=>{
    if(mode == "text"){
        if(drawing){
            var key = event.keyCode;
            if(key == 13){
                drawing = false;
                return;
            }
            else{
                txt_content += String.fromCharCode(key);
                ctx.clearRect(0, 0, c.width, c.height);
                ctx.putImageData(img_before_txt, 0, 0);
                ctx.fillText(txt_content, posX, posY);
                pushImage();
            }
        }
    }
}, true);
color_picker.addEventListener("change", ()=>{
    color = "#" + color_picker.value;
});
brush_size.addEventListener("change", ()=>{
    size = brush_size.value;
});
font_selecter.addEventListener("change", ()=>{
    txt_font = font_selecter.value;
});
function refresh(){
    ctx.clearRect(0, 0, c.width, c.height);
    pushImage();
}
function pushImage(){
    var length = ImageArray.length;
    for(var i = length-1; i > step; i--)
        ImageArray.pop();
    step++;
    ImageArray.push(ctx.getImageData(0, 0, c.width, c.height));
}
function undo(){
    if (step > 0) {
        step--;
        load_curImage();
    }
}
function redo() {
    if (step < (ImageArray.length-1)) {
        step++;
        load_curImage();
    }
}  
function load_curImage(){
    var imageData = ImageArray[step];
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.putImageData(imageData, 0, 0);
}
function change_mode(m){
    mode = m;
}



function fill_mode(){
    if(filling) filling = false;
    else filling = true;
}
function download_img(){
    image = c.toDataURL("image/png").replace("image/png", "image/octet-stream");
    var link = document.createElement('a');
    link.download = "myImage.png";
    link.href = image;
    link.click();
  }




  