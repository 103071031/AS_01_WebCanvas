# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
1. basic brush:
    * click down the mouse and move it to stroke
 
    * id: pen;  https://img.icons8.com/material-rounded/24/000000/border-color.png
    * change mode to "pen" when click the button
    * add event listener to canvas :
    * mousedown => allow canvas can be drawn
    * mousemove => stroke where the mouse moves on if canvas can be drawn 
    * mouseup => set the canvas cannot be drawn

2. eraser: 
    * click down the mouse and move it to erase
    
    * id: eraser; https://img.icons8.com/ios/32/000000/erase-filled.png
    * change mode to "eraser" when click the button
    * add event listener to canvas :
    * mousedown => allow canvas can be erased
    * mousemove => erased where the mouse moves on if canvas can be erased 
    * mouseup => set the canvas cannot be erased
    * use the method as same as the basic brush 
    * the only difference is that the globalCompositeOperation of the canvas context is changed from "source-over" to "destination-out"
    * turn the brush color to transparent
3. Text input:
    * click where you want to type on canvas and key in
    * you can use undo button to backspace 
    * if you keydown the "enter", the canvas will be set to cannot be drawn
    * you can click other somewhere to type

    * id: text https://img.icons8.com/windows/32/000000/type.png
    * addEventListener to canvas :
    * mouseclick => set font and style of text and allow the canvas can be typed
    * addEventListener to window :
    * keydown => save what the user type as a string and use fillText() to draw
               
4. color:
    * default color is black 
    * pick a color and the color style of all brush will be changed

    * id: colorpicker
    * use input tag in HTML  
    * /// use the jscolor.js to implement the color picker:  (http://jscolor.com/examples/)
    * addEventListener to colorpicker 
    * change => change the brush color
5. brush size:
    * choose the brush size and all the brush size and the text size will be changed

    * id: brushsize
    * addEventListener to brushsize
    * change => change the brush size
6. text font:
    * select the text font 

    * id: fontselecter https://img.icons8.com/ios/24/000000/broom-filled.png
    * use select tag in HTML
    * addEventListener to fontselecter 
    * change => change the text font
7. refresh: 
    * you can clear the canvas 

    * id: refresh https://img.icons8.com/material-outlined/24/000000/restart.png
    * use clearRect to clear the canvas area
8. shape brush:
    * <1> rectangle : click down the mouse and move the cursor to measure the size
                     
        
        *  **id: rec https://img.icons8.com/windows/32/000000/rectangle-stroked.png**
        * add event listener to canvas :
            * mousedown => allow canvas can be drawn
            * mousemove => measure the size of the rectangle when the mouse moves if canvas can be drawn 
            * mouseup => drawing complete and set the canvas cannot be drawn

    * <2> circle : click down the mouse and move the cursor to measure the size and when mouseup the circle will appeared
        
        *  **id: circle https://img.icons8.com/windows/32/000000/0-percents.png**
        * add event listener to canvas :
            * mousedown => allow canvas can be drawn
            * mousemove => measure the size of the circle when the mouse moves if canvas can be drawn 
            * mouseup => drawing complete and set the canvas cannot be drawn

    * <3> triangle :  click down the mouse and move the cursor to measure the size and when mouseup the triangle will appeared

        * **id: tri https://img.icons8.com/windows/32/000000/triangle-stroked.png**
        * add event listener to canvas :
            * mousedown => allow canvas can be drawn
            * mousemove => measure the size of the triangle when the mouse moves if canvas can be drawn 
            * mouseup => drawing complete and set the canvas cannot be drawn
9. Undo:
    * back to the previous step or delete the element you type
    * id: undo https://img.icons8.com/material-outlined/24/000000/undo.png
10. Redo:
    * go to the next step 
    * id: redo https://img.icons8.com/material-outlined/24/000000/redo.png

    * => use getImageData() to save the current state of canvas into an array everytime the user draw an element on canvas 
    *    use a variable "step" to record the cur state
    * undo : step--
    * redo step++

11. download :
    * download your canvas to a .png image

    * id: download https://img.icons8.com/material-outlined/24/000000/download-from-cloud.png
    * use toDataURL to change the canvas context to .png url
    * use document.createElement('a') to create an "a" element in HTML document
    * and set the image name to myImage.png
    * then click the link in the function
12. fill:
    * if you click this button 
    * you can draw rectangle, circle which are filled with the color you picked
    * if you click it again 
    * the brush will return to the original mode

    * id: fill https://img.icons8.com/ios/24/000000/fill-color-filled.png
    * use a variable "filling" to record whether the shape is filled 
    * default : not filled
    * if not filled => stroke()
    * if filled => fill()

13. cursor:
    * when the cursor is on the button it will change to pointer
    * when the cursor is on the canvas it will change to crosshair
14. Reference:
    https://www.w3schools.com/
    https://developer.mozilla.org/zh-TW/docs/Web/API/Canvas_API
    https://qiita.com/cotsdrop/items/c5924842aac4ca6e8d54
    https://khanacademy.zendesk.com/hc/en-us/community/posts/202342544-User-Input-in-JavaScript-by-canvas
    https://arayzou.com/2013/10/21/JS%E7%9B%91%E5%90%AC%E9%94%AE%E7%9B%98%E6%8C%89%E9%94%AE%E6%89%A7%E8%A1%8C%E4%BA%8B%E4%BB%B6%E3%80%90%E9%99%84Keycode%E5%AF%B9%E7%85%A7%E8%A1%A8%E3%80%91/
    http://jscolor.com/examples/
    https://icons8.com/icons
    https://www.sanwebe.com/snippet/downloading-canvas-as-image-dataurl-on-button-click